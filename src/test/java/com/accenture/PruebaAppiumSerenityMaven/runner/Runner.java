package com.accenture.PruebaAppiumSerenityMaven.runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@CucumberOptions(
		features = "src/test/resources/features" ,
		glue = {"com.accenture.PruebaAppiumSerenityMaven.stepDefinitions" })
@RunWith(CucumberWithSerenity.class)
public class Runner {
	
}
